// 1) дії які можна виконати над об'єктами, функції, які зберігаються як властивості об'єкта
// 2) будь-якого типу
// 3) змінна, що містить тип посилання, не містить його фактичного значення. Вона містить посилання на місце у пам'яті, де розміщуються реальні дані.

function createNewUser() {

    const newUser = {
        getLogin: function () {
            return `${(this.firstName[0] || '').toLowerCase()}${this.lastName.toLowerCase()}`;
        },

        setFirstName: function (firstName) {
            Object.defineProperty(this, 'firstName', { writable: true, enumerable: true, configurable: true });
            this.firstName = firstName;
            Object.defineProperty(this, 'firstName', { writable: false, enumerable: true, configurable: true });
        },

        setLastName: function (lastName) {
            Object.defineProperty(this, 'lastName', { writable: true, enumerable: true, configurable: true });
            this.lastName = lastName;
            Object.defineProperty(this, 'lastName', { writable: false, enumerable: true, configurable: true });
        },
    };

    Object.defineProperties(newUser, {
        firstName: {
            value: prompt(`Enter your name`) || '',
            writable: false,
            configurable: true,
            enumerable: true,
        },
        lastName: {
            value: prompt(`Enter your last name`) || '',
            writable: false,
            configurable: true,
            enumerable: true,
        },
    });

    return newUser;
}

const newUser = createNewUser();
console.log(newUser.getLogin());

newUser.firstName = 'Hello';
console.log(newUser.firstName, '- Не змінилось');

newUser.setFirstName('Hello');
console.log(newUser.firstName, '- Змінилось!');